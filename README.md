# Calculus project for matlab

Clone this repo and open the folder in `matlab` to run the codes.

Main files name starts with `ab_lab`, run these files in respective folders.

## [Lab 01](lab01) 

This lab is about learning how to do integral and matlab functions. 

## [Lab 02](lab02)

This lab is about doing differential equations.
