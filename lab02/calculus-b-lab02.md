# Calculus B - Lab 2 notes

## About

This is a "lab" using [matlab](https://www.mathworks.com/) at Malmö University on the course **Calculus B**. This is part 2 of the lab and it's about differential equations. The first part is about integrals.

The document is written using Markdown. Not all Markdown renderer can render the equations, there will be broken text.

Check the `ab_lab02.m` file for the code in the [repo](https://gitlab.com/kthexiii-learning-matlab/calculus-b-labs).

## Table of Contents
- [Calculus B - Lab 2 notes](#calculus-b---lab-2-notes)
  - [About](#about)
  - [Table of Contents](#table-of-contents)
  - [1. Parachute](#1-parachute)
  - [2. Pendulum](#2-pendulum)
    - [2.a. Second order to first order](#2a-second-order-to-first-order)
    - [2.b. Solve using ode45 in `matlab`.](#2b-solve-using-ode45-in-matlab)
  - [3. Beam](#3-beam)
    - [3.a. Get x = 20 and x = 50 at y(L) = 0](#3a-get-x--20-and-x--50-at-yl--0)
    - [3.b](#3b)
  - [4. Spring](#4-spring)
    - [4.a. Rewrite equation to first order differential equation](#4a-rewrite-equation-to-first-order-differential-equation)
    - [4.b Solve equation](#4b-solve-equation)

## 1. Parachute

$m = \mathrm{100\ kg}$

$v_0 = \mathrm{20\ m/s}$

$k  = 40\ \mathrm{Ns^2}/m^2$

$g = 9.82\ m/s^2$

$$
F(t) = mg - kv(t)^2
$$

Newton's second law of motion $F(t) = ma(t) = mv'(t)$

$$
\begin{cases}
  v' = g - \frac{k}{m} v^2 \\
  v(0) = v_0 = 20
\end{cases}
$$

Find the general equation

$$
v' = b - av^2 \\
\frac{dv}{dt} = b - av^2\\
\frac{dv}{dt} = a \left (\frac{b}{a} - v^2 \right )
$$

Let $k^2 = \frac{b}{a}$

$$
\frac{dv}{dt} = a \left( k^2 - v^2 \right ) \\
\frac{dv}{dt} = -a \left( v^2 - k^2 \right ) \\
\int{\frac{dv}{v^2 - k^2}} = -a \int{dt} \\
\int{\frac{dv}{(v-k)(v+k)}} = -at + c_1 \\
= \frac{1}{2k} \int{ \left [ \frac{1}{v-k} - \frac{1}{v+k} \right] dv} = -at + c_1
$$

Simplify

$$
\frac{1}{v-k} - \frac{1}{v+k} \\
= \frac{v + k - (v - k)}{(v-k)(v+k)} \\ =
\frac{2k}{(v-k)(v+k)}
$$

Continuing

$$
\frac{1}{2k} \int{ \left [ \frac{1}{v-k} - \frac{1}{v+k} \right] dv} = -at + c_1\\
\frac{1}{2k} \left [ \ln{[v - k]} - \ln{[v+k]} \right] = -at + c_1 \\
\frac{1}{2k} \ln{\left ( \frac{v-k}{v+k}\right)} = -at + c_1 \\
\ln{\left ( \frac{v-k}{v+k}\right)} = -at\cdot 2k + c_1 \\
e^{\ln{\left ( \frac{v-k}{v+k}\right)}} = e^{-at\cdot 2k + c_1} \\
\frac{v-k}{v+k} = c e^{-at\cdot 2k} \\
v - k = (v + k)ce^{-at\cdot 2k} \\
v - v\left(ce^{-at\cdot 2k}\right) = k\left( ce^{-at\cdot 2k} \right) + k \\
v\left(1 - ce^{-at\cdot 2k}\right) = k(ce^{-at\cdot 2k} + 1) \\
v = \frac{k(ce^{-at\cdot 2k} + 1)}{1 - ce^{-at\cdot 2k}}
$$


General equation

$$
v = \frac{\sqrt{\frac{b}{a}} \left( ce^{-2at\sqrt{\frac{b}{a}}} + 1\right)}{1 - ce^{-2at\sqrt{\frac{b}{a}}}}
$$

Replace $a$ and $b$ to the correct variable

$$
v = \frac{\sqrt{\frac{mg}{k}} \left( ce^{-2\frac{k}{m}t\sqrt{\frac{mg}{k}}} + 1\right)}{1 - ce^{-2\frac{k}{m}t\sqrt{\frac{mg}{k}}}}
$$

Solve equation when $v(0) = 20$

$$
v(0) = \frac{\sqrt{\frac{mg}{k}} \left( ce^{-2\frac{k}{m}0\sqrt{\frac{mg}{k}}} + 1\right)}{1 - ce^{-2\frac{k}{m}0\sqrt{\frac{mg}{k}}}} = 20 \\
v(0) = \frac{\sqrt{\frac{mg}{k}}(c + 1)}{1 - c} = 20 \\
\frac{20}{\sqrt{\frac{mg}{k}}} = \frac{c+1}{1-c} \\
\frac{20}{\sqrt{\frac{mg}{k}}} - \frac{20}{\sqrt{\frac{mg}{k}}}c = 1 + c \\
c + \frac{20}{\sqrt{\frac{mg}{k}}}c = \frac{20}{\sqrt{\frac{mg}{k}}} - 1 \\
c \left(1 + \frac{20}{\sqrt{\frac{mg}{k}}}  \right) = \frac{20}{\sqrt{\frac{mg}{k}}} - 1 \\
c = \frac{ \frac{20}{\sqrt{\frac{mg}{k}}} - 1}{1 + \frac{20}{\sqrt{\frac{mg}{k}}} }
$$

$$
c = 0.602
$$

Let $t \rightarrow \infty$, $v = \sqrt{\frac{mg}{k}}$ 
Let $t = 0$, $v = 20$

$v' = 0$, $t \rightarrow \infin$

## 2. Pendulum

Angle $y(t)$, with gravity $F(t) = -cy'(t)$, $c$ is the dampining constant.

$$
\begin{cases}
  y'' + \frac{c}{mL}y' + \frac{g}{L}\sin y = 0 \\
  y(0) = a \\
  y'(0) = b
\end{cases}
$$

$y(0) = a$, is the angle and $y'(0) = b$, is the angular velocity at $t = 0$

### 2.a. Second order to first order

Time intervall 0 to 10, change the variable to $y = y_1$ and $y' = y_2$

$$
\begin{cases}
  y'_1 = y_2 \\
  y'_2 = -\frac{c}{mL}y_2 - \frac{g}{L}\sin y_1 \\
  y_1(0) = a \\
  y_2(0) = b
\end{cases}
$$

### 2.b. Solve using [ode45](https://mathworks.com/help/matlab/ref/ode45.html) in `matlab`.

$g/L = 1$

$c/(mL) = 0.3$

$a = \pi / 2$, $b = 0$

Time range `[0 30]`.

## 3. Beam

$$
y'' - \frac{T}{EI}y = \frac{wx(x-L)}{2El},\ 0 \leq x \leq L.
$$

$x = 0$ and $x=L$

$$
y(0) = y(L) = 0
$$

Assume

$L = 100$, $w = 100$, $E = 10^2$, $T = 500$, $I = 500$ 

Convert second order to first order

$$
\begin{cases}
  y'_1 = y_2 \\
  y'_2 = \frac{T}{EI}y_1 + \frac{wx(x-L)}{2EL} \\
  y_1(0) = 0 \\
  y_2(0) = y_{guess}
\end{cases}
$$

### 3.a. Get x = 20 and x = 50 at y(L) = 0

With $w = 100$
$x_{20} = 0.0188$
$x_{50} = 0.0344$

### 3.b

With $w = 200$
$x_{20} = 0.0176$
$x_{50} = 0.01875$

## 4. Spring

$$
F(t) = ma(t) \Leftrightarrow -kx(t) = mx''(t)
$$

$$
-kx(t) = mx''(t) \Leftrightarrow x''(t) + \frac{k}{m}x(t) = 0
$$

$x(t)$ is the distance.

$k$ is the constant factor characteristic of the spring.

Quadratic formula

$$
r^2 + \frac{k}{m} = 0 \Leftrightarrow r = \plusmn i \sqrt{\frac{k}{m}}
$$

Equation solves to

$$
x(t) = C\sin(\mu t + \delta)
$$

$\mu = \sqrt{\frac{k}{m}}$ is the system's frequency.

Apply the external force $F(t) = A\cos(\omega t)$ and you'll get differential equation.

$$
-kx(t) + A\cos(\omega t) = mx''(t) \Leftrightarrow x''(t) + \frac{k}{m}x(t) = \frac{A}{m}\cos(\omega t)
$$

### 4.a. Rewrite equation to first order differential equation

$$
x''(t) + 0 \cdot x'(t) +\frac{k}{m}x(t) = \frac{A}{m}\cos(\omega t)
$$

$$
\begin{cases}
  x''(t) + 0 \cdot x'(t) +\frac{k}{m}x(t) = \frac{A}{m}\cos(\omega t) \\
  x(0) = a \\
  x'(0) = b
\end{cases}
$$
Assume $x'=x_2$ and $x=x_1$

$$
\begin{cases}
  x'_1 = x_2 \\
  x\,'_2 = \frac{A}{m}\cos(\omega t) - \frac{k}{m}x_1(t)  \\
  x_1(0) = a \\
  x_2(0) = b
\end{cases}
$$

### 4.b Solve equation

Condition

$m = k = a = 1$ 

$x(0) = x'(0) = 0$

time range `[0 30]`

$$
x'_2 = cos(\omega t) - x_1(t)
$$