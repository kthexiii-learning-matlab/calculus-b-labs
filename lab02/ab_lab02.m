%  ab_lab02.m
%
%  Created by Pratchaya Khansomboon on 2020-04-17.
%  Copyright © 2020 Pratchaya Khansomboon. All rights reserved.

clc
clear
close all
format long

% TASK - 01: Parachute
fprintf('\x1b[32mTASK - 01\x1b[0m: Parachute\n')

m = 100;
g = 9.82;
k = 40;
f = @(t, v) g - ( k / m ) .* v^2;

tr = [0 10]; % time range
v0 = 20; % m/s

% This is solved with the intial value after solving for general function
c = (20/sqrt(m*g/k) - 1)/(1 + 20/sqrt(m*g/k));
% General function 
ef = @(x) c * exp((-2 * k) / m .* x .* sqrt((m*g)/k)); 
genf = @(x) (sqrt((m*g)/k) .* (ef(x) + 1))./(1 - ef(x));

% Create an array from a to b
x = linspace(0, 10);

% Solve differential equation with ODE45
[t, y] = ode45(f, tr, v0);

fprintf('\t\x1b[33mfigure\n\n')
figure
plot(t, y) % plot the ODE solved values
hold on

% Plot the general function
plot(x, genf(x), '--');

grid on

title('TASK - 01: Parachute')
ylabel('velocity (m/s)')
xlabel('time (s)')

% TASK - 02: Pendulum
fprintf('\x1b[32mTASK - 02\x1b[0m: Pendulum\n')

tr = [0 30];
gL = 1;
cmL = 0.3;

a = pi/2;
b = 0;

f = @(x, y) [y(2) ; -cmL .* y(2) - gL * sin(y(1))];

[x, y] = ode45(f, tr, [ a ; b]);

fprintf('\t\x1b[33mfigure\x1b[0m\n\n')
figure
plot(x, y(:,1));
title('TASK - 02: Pendulum')
xlabel('time')
ylabel('angle')

% TASK - 03: Beam
fprintf('\x1b[32mTASK - 03\x1b[0m: Beam\n')
L = 100;
w = 100;
E = 10^7;
T = 500;
I = 500;
f = @(x, y) [y(2); y(1)*T/(E*I) + w*x*(x-L)/(2*E*I)];

yg = 0.001;
tr = 0 : 0.1 : 100;

[x, y] = ode45(f, tr, [0 ; yg]);

fprintf('\t\x1b[33mfigure: \x1b[37m1\x1b[0m\n')
figure
plot(x, y(:,1));
hold on

fprintf('\t\x1b[32mx = 20: \x1b[37m%.10f\x1b[0m\n', y(20,1))
fprintf('\t\x1b[32mx = 50: \x1b[37m%.10f\x1b[0m\n\n', y(50,1))

xline(20)
xline(50)

w = 200;
f = @(x, y) [y(2); y(1)*T/(E*I) + w*x*(x-L)/(2*E*I)];
[x, y] = ode45(f, tr, [0 ; yg]);

fprintf('\t\x1b[33mfigure: \x1b[37m2\x1b[0m\n')
plot(x, y(:,1));
fprintf('\t\x1b[32mx = 20: \x1b[37m%.10f\x1b[0m\n', y(20,1))
fprintf('\t\x1b[32mx = 50: \x1b[37m%.10f\x1b[0m\n\n', y(50,1))

legend('w = 100', 'w = 200')
title('TASK - 03: Beam example')
xlabel('length')
ylabel('deflection')

% TASK - 04: Spring
fprintf('\x1b[32mTASK - 04\x1b[0m: Spring\n')

tr = [0 200];

w = sqrt(1/1);

f = @(t, x) [x(2) ; cos(w*t)-x(1)];

[x, y] = ode45(f, tr, [0 ; 0]);

fprintf('\t\x1b[33mfigure\x1b[0m\n\n')
figure
plot(x, y(:,1));
hold on

w = 0.9;
f = @(t, x) [x(2) ; cos(w*t)-x(1)];
[x, y] = ode45(f, tr, [0 ; 0]);

plot(x, y(:,1))

legend("w = 1", "w = 0.9")
title('TASK - 04: Spring')
xlabel('time')
ylabel('length')
