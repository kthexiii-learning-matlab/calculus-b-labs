%  ab_lab01.m
%
%  Created by Pratchaya Khansomboon on 2020-04-16.
%  Copyright © 2020 Pratchaya Khansomboon. All rights reserved.

clc
clear
close all
format long

% PROBLEM - 01: Integral
fprintf('PROBLEM - 01: Integral\n')
x = linspace(0, pi);
f = @(x) log(1 + sin(x)) .* cos(x);

a = 0;
b = pi/2;

fprintf('    10:\t\t%.15f\n', trapets(f, a,b, 10))
fprintf('    100:\t%.15f\n', trapets(f, a,b, 100))
fprintf('    1000:\t%.15f\n', trapets(f, a,b, 1000))
fprintf('    Exact:\t%.15f\n', 2*log(2)-1)

figure
plot(x,f(x))
hold on
x = linspace(a, b);
area(x,f(x), 'FaceColor', '#c3ff82', 'LineStyle', 'None')

title('PROBLEM - 01: Integral')
xlabel('x')
ylabel('y')

% PROBLEM - 02: General Integral
fprintf('\nPROBLEM - 02: General Integral\n')

f = @(x) exp(-x) .* cos(x);
x = linspace(0, 10*pi);

% NOT IMPLEMENTED
fprintf('    ans: -e^(-2pi)/2+(1/2)\n')
fprintf('    approx: %.15f\n', gint(f, 0, 2))

figure
plot(x,f(x))
hold on
area(x,f(x), 'FaceColor', '#c3ff82', 'LineStyle', 'None')

title('PROBLEM - 02: General Integral')
xlabel('x')
ylabel('y')

% PROBLEM - 03: 
fprintf('\nPROBLEM - 03\n')

a = 0;
b = 4*pi;

t = linspace(a, b);
x = @(t) 5 .* t - 5 * sin(t); 
y = @(t) 5 - 5 .* cos(t);

f = @(t) 5.*(sin(t).^2 + (5.*cos(t) - 5).^2/25).^(1/2);

figure
plot(x(t),y(t))

title('PROBLEM - 03: Curve')
xlabel('x')
ylabel('y')

fprintf('    03a: Figure 3\n')
fprintf('    03b: %.3f\n', quadl(f, a, b, 1e-15))
fprintf('    03c: 80\n')

% PROBLEM - 04: 
fprintf('\nPROBLEM - 04\n')
a = 0.1;
b = 2;

x = linspace(-5, 5);
f = @(x) a .* x.^2 + b;
fp = @(x) (x.^2/25 + 1).^(1/2);

fprintf('    04: %.15f\n', quadl(fp, -5, 5, 1e-15))

figure
plot(x,f(x))
hold on
plot([-5 -5], [0 4.5])
plot([5 5], [0 4.5])
plot([-5 5], [2 2])
plot([-4 -4], [f(-4) 2])
plot([-3 -3], [f(-3) 2])
plot([-2 -2], [f(-2) 2])
plot([-1 -1], [f(-1) 2])

plot([1 1], [f(1) 2])
plot([2 2], [f(2) 2])
plot([3 3], [f(3) 2])
plot([4 4], [f(4) 2])

axis([-6 6 0 5])

title('PROBLEM - 04: Bridge')
xlabel('x')
ylabel('y')