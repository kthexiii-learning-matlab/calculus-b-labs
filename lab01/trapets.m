function T = trapets(fun, a, b, n)
%trapets - Calculate area under a function using 
%
% Syntax: T = trapets(fun, a, b, n)
%
% 

  h = (b - a) / n;
  x = linspace(a, b, n + 1);
  f = fun(x);
  T = h * (sum(f(2:n)) + 0.5 * (f(1) + f(n+1)));

end