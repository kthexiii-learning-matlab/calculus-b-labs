<!-- $$
\int^\infty_0 e^{x} \cdot cos(x) dx = -e^{-x} \cdot cos(x -)
$$ -->

$$
\frac{-e^{2\pi}}{2} + \frac{1}{2}
$$