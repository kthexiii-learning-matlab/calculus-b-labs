function [v, X] = gint(f, a, b)
% gint - calculate area under curve to the maxium possible 
% 
% Syntax: gint(f, a, b)
%       f - function
%       a - lower limit
%       b - starting upper limit

  X = b;
  v0 = quadl(f, a, X - 1);
  v = quadl(f, a, X);

  while abs(v - v0) > 1e-13
    v0 = v;
    X = X + 1;
    v = quadl(f, a, X, 1e-16);

    if X == 20000 % just some huge number to iterate to 
      disp("No convergence")
      return
  end
end